package si.uni_lj.fri.pbd.lab2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Intent intent = getIntent();
        String fullName = intent.getStringExtra(RegistrationActivity.EXTRA_NAME);

        TextView nameField = findViewById(R.id.fullName);
        if (nameField == null) {
            // Yes I am using toasts to debug
            // Yes I know that's not how you debug, I will learn the correct way.
            // I just don't have time right now.
            // Give me a break OK?
            Toast toast = Toast.makeText(getApplicationContext(), "Could not find name field!", Toast.LENGTH_LONG);
            toast.show();
        }

        if (nameField != null) {
            nameField.setText(fullName);
        }

        Button modSubmit = (Button) findViewById(R.id.MODsubmit);

        if (modSubmit == null) {
            Toast toast = Toast.makeText(getApplicationContext(), "Could not find submit button!", Toast.LENGTH_LONG);
            toast.show();
        }

        modSubmit.setOnClickListener(view -> publishMOD(view));
    }

//private:
    private void publishMOD(View view) {
        EditText modField = findViewById(R.id.messageOfTheDay);
        if (modField == null) {
            Toast toast = Toast.makeText(getApplicationContext(),"Your phone has been hacked, please factory reset ALL settings immediately!", Toast.LENGTH_LONG);
            toast.show();
        }

        String mod = modField.getText().toString().trim();

        if (mod.length() > 0) {
            Toast toast = Toast.makeText(getApplicationContext(), mod, Toast.LENGTH_LONG);
            toast.show();
        }
        modField.setText("");
    }
}
