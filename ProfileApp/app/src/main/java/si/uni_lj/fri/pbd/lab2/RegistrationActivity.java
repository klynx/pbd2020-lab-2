package si.uni_lj.fri.pbd.lab2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class RegistrationActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
    }

//public:
    public static String EXTRA_NAME;

    public void registerUser(View view) {
        //EditText nameField = (EditText) view.findViewById(R.id.edit_name);
        EditText nameField = (EditText) findViewById(R.id.edit_name);

        if (nameField == null) {
            // Yes I am using toasts to debug
            // Yes I know that's not how you debug, I will learn the correct way.
            // I just don't have time right now.
            // Give me a break OK?
            Toast toast = Toast.makeText(getApplicationContext(), "Could not find name field!", Toast.LENGTH_LONG);
            toast.show();
            return;
        }

        String fullName = nameField.getText().toString();

        if (fullName.trim().length() <= 0) {
            String errorMsg = getString(R.string.reg_full_name_error);
            nameField.setError(errorMsg);
        }
        else {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra(EXTRA_NAME, fullName);
            startActivity(intent);
        }
    }

}
